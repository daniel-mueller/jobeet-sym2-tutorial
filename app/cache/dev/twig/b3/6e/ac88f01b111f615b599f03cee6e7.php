<?php

/* WebProfilerBundle:Profiler:bag.html.twig */
class __TwigTemplate_b36eac88f01b111f615b599f03cee6e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table ";
        if (array_key_exists("class", $context)) {
            echo "class='";
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
            echo "'";
        }
        echo " >
    <thead>
        <tr>
            <th scope=\"col\">Key</th>
            <th scope=\"col\">Value</th>
        </tr>
    </thead>
    <tbody>
        ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_sort_filter($this->getAttribute((isset($context["bag"]) ? $context["bag"] : $this->getContext($context, "bag")), "keys")));
        foreach ($context['_seq'] as $context["_key"] => $context["key"]) {
            // line 10
            echo "        <tr>
            <th>";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["key"]) ? $context["key"] : $this->getContext($context, "key")), "html", null, true);
            echo "</th>
            <td>";
            // line 12
            echo twig_escape_filter($this->env, twig_jsonencode_filter($this->getAttribute((isset($context["bag"]) ? $context["bag"] : $this->getContext($context, "bag")), "get", array(0 => (isset($context["key"]) ? $context["key"] : $this->getContext($context, "key"))), "method")), "html", null, true);
            echo "</td>
        </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['key'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "    </tbody>
</table>
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:bag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 19,  58 => 17,  127 => 60,  110 => 22,  76 => 28,  113 => 36,  90 => 32,  81 => 21,  59 => 13,  53 => 11,  159 => 11,  148 => 5,  102 => 17,  100 => 54,  97 => 41,  23 => 1,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  235 => 74,  229 => 73,  224 => 71,  220 => 70,  214 => 69,  208 => 68,  169 => 14,  143 => 56,  140 => 55,  132 => 51,  128 => 33,  119 => 39,  107 => 36,  38 => 6,  71 => 17,  177 => 65,  165 => 64,  160 => 61,  135 => 62,  126 => 43,  114 => 42,  84 => 29,  70 => 20,  67 => 15,  61 => 12,  28 => 3,  196 => 90,  183 => 70,  171 => 61,  166 => 13,  163 => 70,  158 => 79,  156 => 10,  151 => 6,  142 => 59,  138 => 57,  136 => 56,  121 => 46,  117 => 19,  105 => 18,  91 => 50,  62 => 14,  49 => 10,  94 => 34,  89 => 20,  85 => 32,  75 => 18,  68 => 14,  56 => 11,  24 => 3,  87 => 20,  25 => 35,  21 => 2,  31 => 3,  26 => 9,  19 => 1,  93 => 23,  88 => 31,  78 => 26,  46 => 12,  44 => 12,  27 => 4,  79 => 18,  72 => 16,  69 => 25,  47 => 8,  40 => 6,  37 => 5,  22 => 2,  246 => 32,  157 => 56,  145 => 46,  139 => 63,  131 => 61,  123 => 59,  120 => 20,  115 => 61,  111 => 59,  108 => 19,  101 => 43,  98 => 31,  96 => 31,  83 => 22,  74 => 27,  66 => 15,  55 => 15,  52 => 10,  50 => 9,  43 => 7,  41 => 10,  35 => 9,  32 => 4,  29 => 3,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 62,  173 => 61,  168 => 66,  164 => 59,  162 => 62,  154 => 54,  149 => 51,  147 => 58,  144 => 53,  141 => 51,  133 => 55,  130 => 71,  125 => 69,  122 => 43,  116 => 36,  112 => 42,  109 => 41,  106 => 45,  103 => 27,  99 => 30,  95 => 34,  92 => 33,  86 => 23,  82 => 28,  80 => 30,  73 => 16,  64 => 13,  60 => 16,  57 => 12,  54 => 22,  51 => 14,  48 => 9,  45 => 8,  42 => 11,  39 => 10,  36 => 5,  33 => 4,  30 => 3,);
    }
}
